<?php

	session_start();
    if (!isset($_SESSION['login'])) $_SESSION['login'] = 0;
    
	if (isset($_REQUEST['action'], $_REQUEST['param']) && strtolower($_REQUEST['action']) == 'login' && is_array($_REQUEST['param']) && count($_REQUEST['param']) > 1) {
    	switch(true) {
        	case $_REQUEST['param'][0] == 'admin1' && $_REQUEST['param'][1] == 'pass1':
            	$_SESSION['login'] = 1;
            	break;
        	case $_REQUEST['param'][0] == 'user1' && $_REQUEST['param'][1] == 'pass1':
            	$_SESSION['limit'] = array('domain1.de', 'domain2.de', 'domain3.de');
            	$_SESSION['login'] = 1;
            	break;
        	default: 
                break;
        }
		header('Content-Type: application/json');
        echo json_encode(array('state' => $_SESSION['login']));
        exit;
    }
    
    if ($_SESSION['login'] == 1 && isset($_REQUEST['action'])) {
    	switch (strtolower($_REQUEST['action'])) {
    		case 'logout':
				session_destroy();
				header('Content-Type: application/json');
		        echo json_encode(array('state' => 1));
				exit;
			case 'getdomainlist':
				$result = array();
				$f = @opendir('/etc/bind/zones/master');
				if ($f) {
					while (($file = readdir($f)) !== false) {
						if ($file[0] != '.' && is_dir("/etc/bind/zones/master/$file") && (!isset($_SESSION['limit']) || in_array($file, $_SESSION['limit']))) {
							$result[] = $file;
						}
					}
					closedir($f);
				}
				header('Content-Type: application/json');
				echo json_encode(array('state' => 1, 'type' => 'domainlist', 'data' => $result));
				exit;
			case 'getzonedata':
				header('Content-Type: application/json');
				if (isset($_REQUEST['param']) && is_array($_REQUEST['param']) && count($_REQUEST['param']) > 0) {
					$zonefile = "/etc/bind/zones/master/" . $_REQUEST['param'][0] . "/" . $_REQUEST['param'][0];
					if (file_exists($zonefile)) {
						echo json_encode(array('state' => 1, 'type' => 'zonedata', 'data' => file_get_contents($zonefile)));
					} else {
						echo json_encode(array('state' => 0, 'message' => 'File does not exist: ' . $zonefile));
					}
				} else {
					echo json_encode(array('state' => 0, 'message' => 'Missing param 0.'));
				}
				exit;
			case 'savezone':
				header('Content-Type: application/json');
				if (isset($_REQUEST['param']) && is_array($_REQUEST['param']) && count($_REQUEST['param']) > 1) {
					$zone = $_REQUEST['param'][0];
					$data = base64_decode($_REQUEST['param'][1]);
					$zonefile = "/etc/bind/zones/master/" . $zone . "/" . $zone;
					$tmpzonefile = "/etc/bind/zones/master/" . $zone . "/" . $zone . ".tmp";
					file_put_contents($tmpzonefile, $data);
					ob_start();
					echo "&gt; sudo named-checkzone $zone $tmpzonefile\r\n";
					passthru("sudo named-checkzone $zone $tmpzonefile 2>&1");
					$out = ob_get_clean();
					if (strpos($out, "\nOK") !== false) {
						$log = $out . "<br><br>";
						file_put_contents($zonefile, $data);
						unlink($tmpzonefile);
						ob_start();
						echo "&gt; cd /etc/bind/zones/master/$zone;\r\n&gt sudo dnssec-signzone -N INCREMENT $zone\r\n";
						passthru("cd /etc/bind/zones/master/$zone; sudo dnssec-signzone -N INCREMENT $zone 2>&1");
						echo "\r\n&gt; sudo service bind9 restart\r\n\r\n";
						passthru("sudo service bind9 restart");
						$out = ob_get_clean();
						$log .= $out;
						echo json_encode(array('state' => 1, 'type' => 'log', 'data' => nl2br($log)));
					} else {
						echo json_encode(array('state' => 0, 'type' => 'log', 'data' => nl2br($out)));
					}
				} else {
					echo json_encode(array('state' => 0, 'message' => 'Missing params.'));
				}
				exit;
		}
    }

?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>DNS Administration</title>
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script src="chosen.jquery.min.js"></script>
        <script src="jquery.cookie.js"></script>
		<link rel="stylesheet" type="text/css" href="chosen.min.css" />
		<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/dark-hive/jquery-ui.css" />
		<style>
			body {
				font-size: 12px;
				background-image: url('carbon.jpg');
				background-size: cover;
				background-attachment:fixed;
				background-color: #666;
				color: #ccc;
			}
			* {
				font-family: Verdana, Arial;
			}
			textarea {
				font-family: 'Lucida Console', 'Courier New', monospace;
			}
			#dialogs {
				display: none;
			}
			.ui-dialog-buttonset .ui-button-text {
				font-size: 12px;
			}
			body .wait {
				color: #888888;
			}
			.ok {
				color: green;
			}
			.fail {
				color: red;
			}
			#error {
				background: #ff5555;
				overflow: hidden;
			}
			td {
				padding: 0 5px 5px 0;
				vertical-align: middle;
			}
			header {
				position: absolute;
				height: 50px;
				left: 0;
				right: 0;
				top: 0;
				text-align: center;
			}
			footer {
				position: absolute;
				height: 20px;
				left: 0;
				right: 0;
				bottom: 0;
			}
			#main {
				position: absolute;
				top: 50px;
				bottom: 20px;
				left: 0;
				right: 0;
				overflow: hidden;
			}
			.login-form,
			.login-form h1,
			.login-form span,
			.login-form input,
			.login-form label {
				margin: 0;
				padding: 0;
				border: 0;
				outline: 0;
			}
			.login-wrapper {
				position: absolute;
			    top: 0;
				bottom: 0;
				left: 50%; 
				width: 1px;
				overflow: visible;
			}
			.login-form {
				position: absolute;
				margin: -107px 0 0 0;
			    top: 50%;
				left: -125px; 
				width: 200px;
				height: 200px;
				padding: 15px 25px 0 25px;
				cursor: default;
				background-color: #141517;			 
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
				border-radius: 5px;			 
				-webkit-box-shadow: 0px 1px 1px 0px rgba(255,255,255, .2), inset 0px 1px 1px 0px rgb(0,0,0);
				-moz-box-shadow: 0px 1px 1px 0px rgba(255,255,255, .2), inset 0px 1px 1px 0px rgb(0,0,0);
				box-shadow: 0px 1px 1px 0px rgba(255,255,255, .2), inset 0px 1px 1px 0px rgb(0,0,0);
			}
			.login-form:before {
				position: absolute;
				top: -12px;
				left: 10px;			 
				width: 0px;
				height: 0px;
				content: '';			 
				border-bottom: 10px solid #141517;
				border-right: 10px solid #141517;
				border-top: 10px solid transparent;
				border-left: 10px solid transparent;
			}
			.login-form h1 {
				line-height: 40px;
				font-family: 'Myriad Pro', sans-serif;
				font-size: 22px;
				font-weight: normal;
				color: #e4e4e4;
			}
			.login-form input[type=text],
			.login-form input[type=password],
			.login-form input[type=submit] {
				line-height: 14px;
				margin: 10px 0;
				padding: 6px 15px;
				border: 0;
				outline: none;			 
				font-family: Helvetica, sans-serif;
				font-size: 12px;
				font-weight: bold;
				text-shadow: 0px 1px 1px rgba(255,255,255, .2);			 
				-webkit-border-radius: 26px;
				-moz-border-radius: 26px;
				border-radius: 26px;			 
				-webkit-transition: all .15s ease-in-out;
				-moz-transition: all .15s ease-in-out;
				-o-transition: all .15s ease-in-out;
				transition: all .15s ease-in-out;
			}
			.login-form input[type=text],
			.login-form input[type=password],
			.js .login-form span {
				color: #686868;
				width: 170px;			 
				-webkit-box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .6);
				-moz-box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .6);
				box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .6);			 
				background: #989898;
				background: -moz-linear-gradient(top,  #ffffff 0%, #989898 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#989898));
				background: -webkit-linear-gradient(top,  #ffffff 0%,#989898 100%);
				background: -o-linear-gradient(top,  #ffffff 0%,#989898 100%);
				background: -ms-linear-gradient(top,  #ffffff 0%,#989898 100%);
				background: linear-gradient(top,  #ffffff 0%,#989898 100%);
			}
			.login-form input[type=text]:hover,
			.login-form input[type=password]:hover {
				-webkit-box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .6), 0px 0px 5px rgba(255,255,255, .5);
				-moz-box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .6), 0px 0px 5px rgba(255,255,255, .5);
				box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .6), 0px 0px 5px rgba(255,255,255, .5);
			}
			.login-form input[type=text]:focus,
			.login-form input[type=password]:focus {
				background: #e1e1e1;
				background: -moz-linear-gradient(top,  #ffffff 0%, #e1e1e1 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e1e1e1));
				background: -webkit-linear-gradient(top,  #ffffff 0%,#e1e1e1 100%);
				background: -o-linear-gradient(top,  #ffffff 0%,#e1e1e1 100%);
				background: -ms-linear-gradient(top,  #ffffff 0%,#e1e1e1 100%);
				background: linear-gradient(top,  #ffffff 0%,#e1e1e1 100%);
			}
			.login-form input[type=submit] {
				float: right;
				cursor: pointer;			 
				color: #445b0f;			 
				-webkit-box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .45), 0px 1px 1px 0px rgba(0,0,0, .3);
				-moz-box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .45), 0px 1px 1px 0px rgba(0,0,0, .3);
				box-shadow: inset 1px 1px 1px 0px rgba(255,255,255, .45), 0px 1px 1px 0px rgba(0,0,0, .3);
			}
			.login-form input[type=submit]:hover {
				-webkit-box-shadow: inset 1px 1px 3px 0px rgba(255,255,255, .8), 0px 1px 1px 0px rgba(0,0,0, .6);
				-moz-box-shadow: inset 1px 1px 3px 0px rgba(255,255,255, .8), 0px 1px 1px 0px rgba(0,0,0, .6);
				box-shadow: inset 1px 1px 3px 0px rgba(255,255,255, .8), 0px 1px 1px 0px rgba(0,0,0, .6);
			}
			.login-form input[type=submit]:active {
				-webkit-box-shadow: none;
				-moz-box-shadow: none;
				box-shadow: none;
			}
			.login-form input[type=submit],
			.js .login-form span.checked:before {
				background: #a5cd4e;
				background: -moz-linear-gradient(top,  #a5cd4e 0%, #6b8f1a 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a5cd4e), color-stop(100%,#6b8f1a));
				background: -webkit-linear-gradient(top,  #a5cd4e 0%,#6b8f1a 100%);
				background: -o-linear-gradient(top,  #a5cd4e 0%,#6b8f1a 100%);
				background: -ms-linear-gradient(top,  #a5cd4e 0%,#6b8f1a 100%);
				background: linear-gradient(top,  #a5cd4e 0%,#6b8f1a 100%);
			}
			.js .login-form input[type=checkbox] {
				position: fixed;
				left: -9999px;
			}
			.login-form span {
				position: relative;
				margin-top: 15px;
				float: left;
			}
			.js .login-form span {
				width: 16px;
				height: 16px;
				cursor: pointer;
				-webkit-border-radius: 2px;
				-moz-border-radius: 2px;
				border-radius: 2px;
			}
			.js .login-form span.checked:before {
				content: '';
				position: absolute;
				top: 4px;
				left: 4px;
				width: 8px;
				height: 8px;
				-webkit-box-shadow: 0px 1px 1px 0px rgba(255,255,255, .45), inset 0px 1px 1px 0px rgba(0,0,0, .3);
				-moz-box-shadow: 0px 1px 1px 0px rgba(255,255,255, .45), inset 0px 1px 1px 0px rgba(0,0,0, .3);
				box-shadow: 0px 1px 1px 0px rgba(255,255,255, .45), inset 0px 1px 1px 0px rgba(0,0,0, .3);
			}
			.login-form label {
				position: absolute;
				top: 1px;
				left: 25px;
				font-family: sans-serif;
				font-weight: bold;
				font-size: 12px;
				color: #e4e4e4;
			}
			.main-form {
				display: none;
				height: 100%;
			}
			#zonelist {
				width: 350px;
			}
			footer a {
				color: #ccc;
			}
			#zonedata {
				display: none;
				width: 965px;
				margin-top: 10px;
				height: 425px;
				clear:both;
				font-family: "Lucida Console", Monaco, monospace;
				overflow: scroll;
			}
			#button-save {
				float: right;
				display: none;
			}
			.chosen-drop * {
				color: #000;
			}
			#protocol {
				display: none;
				width: 965px;
				margin-top: 10px;
				height: 425px;
				clear:both;
				font-family: "Lucida Console", Monaco, monospace;
				overflow: scroll;
			}
		</style>
		<script>
			var currentZone = '';
			$(function() {
				//$( "input[type=submit], a, button" ).button();
				$('body').addClass('js');
				// Make the checkbox checked on load
				$('.login-form span').addClass('checked').children('input').attr('checked', true);
				// Click function
				$('.login-form span').on('click', function() {
					if ($(this).children('input').attr('checked')) {
						$(this).children('input').attr('checked', false);
						$(this).removeClass('checked');
					} else {
						$(this).children('input').attr('checked', true);
						$(this).addClass('checked');
					}
				});
				$('#button-logout').click(function(){
					perform_action('logout', [], function(state, type, data) {
						if (state) {
							$('.main-form').fadeOut(500);
							$('header').fadeIn(500);
							$('.login-form').fadeIn(500);
//							$('#zonelist').chosenDestroy();
							$('#zonelist').delay(200).html('');
							$('#login-user').val('');
							$('#login-pass').val('');
							$.removeCookie('dns-admin-user');
							$.removeCookie('dns-admin-pass');

						} else {
							show_fail('Abmeldung fehlgeschlagen.');
						}
					});
				});
				var user = $.cookie('dns-admin-user');
				var pass = $.cookie('dns-admin-pass');
				if (typeof(user) != 'undefined' && $.trim(user) != '' &&typeof(pass) != 'undefined' && $.trim(pass) != '' ) {
					$('#login-user').val(user);
					$('#login-pass').val(pass);
					$('#login-btn').click();
				}
				$('#button-loadzone').click(function(){
					perform_action('getzonedata', [ $('#zonelist').val() ], function(state, type, data) {
						if (state) {
							if ($('#zonedata').css('display') == 'none') {
								$('#zonedata, #button-save').fadeIn(500);
							}
							if ($('#protocol').css('display') != 'none') {
								$('#protocol').hide(0);
							}
							$('#zonedata').val(data);
							currentZone = $('#zonelist').val();
						} else {
							show_fail('Zonendaten konnten nicht geladen werden!');
							$('#zonedata, #button-save').fadeOut(500);
						}
					});
				});
				$('#button-save').click(function(){
					perform_action('savezone', [ currentZone, Base64.encode($('#zonedata').val()) ], function(state, type, data) {
						$('#zonedata, #button-save').fadeOut(500);
						$('#protocol').show(0);
						$('#protocol').html(data);
						if (!state) {
							show_fail('Zonendaten konnten nicht gespeichert werden!');
						}
					});
				});
			});
			var login = function(){
				if ($.trim($('#login-user').val()) == '') {
					if ($.trim($('#login-pass').val()) == '') {
						show_fail('Sie müssen einen Benutzernamen und ein Passwort eingeben!');
					} else {
						show_fail('Sie müssen Benutzernamen eingeben!');
					}
				} else if ($.trim($('#login-pass').val()) == '') {
						show_fail('Sie müssen ein Passwort eingeben!');
				} else {
					perform_action('login', [ $.trim($('#login-user').val()), $.trim($('#login-pass').val()) ], function(state, type, data) {
						if (state) {
							$('.login-form').fadeOut(500);
							perform_action('getDomainList', [], function(state, type, data) {
								if (state && typeof(data) != 'undefined') {
									for (j = 0; j < data.length; j++) {
										$('#zonelist').append('<option>' + data[j] + '</option>');
									}
									$('.main-form').fadeIn(500);
									$('header').fadeOut(500);
									$('#zonelist').chosen();
									if ($('#login-remember').get(0).checked) {
										$.cookie('dns-admin-user', $.trim($('#login-user').val()), { expires: 7 });
										$.cookie('dns-admin-pass', $.trim($('#login-pass').val()), { expires: 7 });
									} else {
										$.removeCookie('dns-admin-user');
										$.removeCookie('dns-admin-pass');
									}
								} else {
									show_fail('Beim Laden der Domainliste trat ein Fehler auf!');
								}
							});
						} else {
							show_fail('Benutzername oder Passwort falsch!');
						}
					});
				}
				return false;
			};
			var show_success = function(message) {
				if (typeof(message) != 'undefined') {
					$( "#dialog-ok .message" ).html(message);
				} else {
					$( "#dialog-ok .message" ).html('Die Aktion war erfolgreich.');
				}
				$( "#dialog-ok" ).dialog({
					resizable:  false,
					height:     typeof(message) == 'undefined' ? 140 : 190,
					width:      300,
					modal:      true,
					buttons:    {
									"OK": function() {
										$( this ).dialog( "close" );
							      	}
					            }
				});
			}
			var show_fail = function(message) {
				if (typeof(message) != 'undefined') {
					$( "#dialog-fail .message" ).html(message);
				} else {
					$( "#dialog-fail .message" ).html('Ein Fehler ist aufgetreten.');
				}
				$( "#dialog-fail" ).dialog({
					resizable:  false,
					height:     typeof(message) == 'undefined' ? 140 : 190,
					width:      300,
					modal:      true,
					buttons:    {
									"OK": function() {
										$( this ).dialog( "close" );
							      	}
					            }
				});
			}
			var perform_action = function(action, params, callback) {
				var data = "action=" + encodeURI(action);
				for (i = 0; i < params.length; i++) {
				    data = data + "&param[]=" + encodeURI(params[i]);
				}
				$.ajax({
					type:       "POST",
					url:        "index.php",
					data:       data,
					success:    function(data) {
									try {
										if (data.state == 1) {
											if (typeof(callback) == 'function') {
												if (data.hasOwnProperty('type') && data.hasOwnProperty('data')) {
													callback(data.state == 1, data.type, data.data);
												} else {
													callback(data.state == 1);
												}
											}
										} else {
											if (typeof(callback) == 'function') {
												callback(false);
											}
										}
									} catch (e) {
										if (typeof(callback) == 'function') {
											callback(false);
										}
									}
					            },
					error:      function(xRo, err, errString) {
									show_fail('Fehler bei der Kommunikation mit dem Server: ' + errString);
					            }
				});
			}
			$.fn.chosenDestroy = function () {
				$(this).show().removeClass('chzn-done').removeAttr('id');
				$(this).next().remove()
				return $(this);
			}
			var Base64 = {
			
				// private property
				_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
				
				// public method for encoding
				encode : function (input) {
					var output = "";
					var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
					var i = 0;
				
					input = Base64._utf8_encode(input);
				
					while (i < input.length) {
				
						chr1 = input.charCodeAt(i++);
						chr2 = input.charCodeAt(i++);
						chr3 = input.charCodeAt(i++);
				
						enc1 = chr1 >> 2;
						enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
						enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
						enc4 = chr3 & 63;
				
						if (isNaN(chr2)) {
							enc3 = enc4 = 64;
						} else if (isNaN(chr3)) {
							enc4 = 64;
						}
				
						output = output +
						this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
						this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
				
					}
				
					return output;
				},
				
				// public method for decoding
				decode : function (input) {
					var output = "";
					var chr1, chr2, chr3;
					var enc1, enc2, enc3, enc4;
					var i = 0;
				
					input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
				
					while (i < input.length) {
				
						enc1 = this._keyStr.indexOf(input.charAt(i++));
						enc2 = this._keyStr.indexOf(input.charAt(i++));
						enc3 = this._keyStr.indexOf(input.charAt(i++));
						enc4 = this._keyStr.indexOf(input.charAt(i++));
				
						chr1 = (enc1 << 2) | (enc2 >> 4);
						chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
						chr3 = ((enc3 & 3) << 6) | enc4;
				
						output = output + String.fromCharCode(chr1);
				
						if (enc3 != 64) {
							output = output + String.fromCharCode(chr2);
						}
						if (enc4 != 64) {
							output = output + String.fromCharCode(chr3);
						}
				
					}
				
					output = Base64._utf8_decode(output);
				
					return output;
				
				},
				
				// private method for UTF-8 encoding
				_utf8_encode : function (string) {
					string = string.replace(/\r\n/g,"\n");
					var utftext = "";
				
					for (var n = 0; n < string.length; n++) {
				
						var c = string.charCodeAt(n);
				
						if (c < 128) {
							utftext += String.fromCharCode(c);
						}
						else if((c > 127) && (c < 2048)) {
							utftext += String.fromCharCode((c >> 6) | 192);
							utftext += String.fromCharCode((c & 63) | 128);
						}
						else {
							utftext += String.fromCharCode((c >> 12) | 224);
							utftext += String.fromCharCode(((c >> 6) & 63) | 128);
							utftext += String.fromCharCode((c & 63) | 128);
						}
				
					}
				
					return utftext;
				},
				
				// private method for UTF-8 decoding
				_utf8_decode : function (utftext) {
					var string = "";
					var i = 0;
					var c = c1 = c2 = 0;
				
					while ( i < utftext.length ) {
				
						c = utftext.charCodeAt(i);
				
						if (c < 128) {
							string += String.fromCharCode(c);
							i++;
						}
						else if((c > 191) && (c < 224)) {
							c2 = utftext.charCodeAt(i+1);
							string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
							i += 2;
						}
						else {
							c2 = utftext.charCodeAt(i+1);
							c3 = utftext.charCodeAt(i+2);
							string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
							i += 3;
						}
				
					}
				
					return string;
				}
			
			}
		</script>
	</head>
	<body>
		<header>
			<h1>DNS Administration</h1>
		</header>
        <div id="main">
	        <div class="login-wrapper">
                <div class="login-form">
                    <h1>Anmeldung</h1>
                    <form onsubmit="return login();">
                        <input type="text" name="username" placeholder="Benutzername" id="login-user">
                        <input type="password" name="password" placeholder="Passwort" id="login-pass">
                        <span>
                            <input type="checkbox" name="checkbox" id="login-remember">
                            <label for="checkbox">Merken</label>
                        </span>
                        <input id="login-btn" type="submit" value="Los geht's">
                    </form>
                </div>
                <div class="main-form">
					<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable" style="height: 600px; width: 1000px; margin-top: -300px; top: 50%; left: -500px; display: block; position: absolute;">
                    	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
                        	<span class="ui-dialog-title">DNS-Administration</span>
						</div>
                        <div class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 0px; max-height: none; height: 519px; padding-top: 15px;">
                        	<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" style="float: right" id="button-logout"><span class="ui-button-text">Abmelden</span></button>
                            <select paceholder="Bitte wählen Sie eine Zone ..." id="zonelist" style="width:350px;">
                            </select>
                        	<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" id="button-loadzone"><span class="ui-button-text">Bearbeiten</span></button>
                            <textarea id="zonedata" wrap="off"></textarea>
                            <div id="protocol"></div>
                        	<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" style="float: right" id="button-save"><span class="ui-button-text">Speichern</span></button>
                            <div style="clear:both"></div>
						</div>
                     </div>
                </div>
            </div>
        </div>
		<div id="dialogs">
			<div id="dialog-ok" title="">
				<p><span class="message"></span></p>
			</div>
			<div id="dialog-fail" title="Fehler">
				<p><span class="message"></span></p>
			</div>
		</div>
        <footer>&copy; <?php echo date("Y"); ?>, <a href="https://www.noltecomputer.com/">RAN COMMUNITY SERVER</a></footer>
	</body>
</html>
